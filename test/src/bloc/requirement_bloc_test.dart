import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:project_management_flutter/src/bloc/requirement_bloc.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/requirement_model.dart';

import 'requirement_bloc_test.mocks.dart';

// Command: flutter pub run build_runner build

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([RequirementBloc])
void main() {
  final _requerimentBlocMock = MockRequirementBloc();
  final _requirementBloc = RequirementBloc();

  var requerimentModel = Requirement(
      id: 1,
      idProject: 1,
      name: 'Creacion de inventario',
      description: 'inventario de pruebas',
      finalDate: '2021-01-01',
      priority: 'LOW',
      status: 'IN_PROCESS',
      observations: '');

  group('REQUERIMENT BLOC TEST', () {
    test('GET REQUIREMENTS', () async {
      var response = Future.value(
          ApiResponse(message: '', object: [], statusResponse: 200));

      when(_requerimentBlocMock.getRequirements())
          .thenAnswer((realInvocation) => response);

      expect(_requirementBloc.getRequirements(), isA<Future<ApiResponse>>());
    });

    test('GET REQUIREMENT BY ID', () async {
      var response = Future.value(ApiResponse(
          message: '', object: requerimentModel, statusResponse: 200));

      when(_requerimentBlocMock.getRequirementById(any))
          .thenAnswer((realInvocation) => response);

      expect(
          _requirementBloc.getRequirementById(1), isA<Future<ApiResponse>>());
    });

    test('STORE REQUIREMENT', () async {
      var response = Future.value(ApiResponse(
          message: '', object: requerimentModel, statusResponse: 200));

      when(_requerimentBlocMock.storeRequirement(requerimentModel))
          .thenAnswer((realInvocation) => response);

      expect(_requirementBloc.storeRequirement(requerimentModel),
          isA<Future<ApiResponse>>());
    });

    test('UPDATE REQUIREMENT', () async {
      var response = Future.value(ApiResponse(
          message: '', object: requerimentModel, statusResponse: 200));

      when(_requerimentBlocMock.updateRequirement(requerimentModel))
          .thenAnswer((realInvocation) => response);

      expect(_requirementBloc.updateRequirement(requerimentModel),
          isA<Future<ApiResponse>>());
    });

    test('DELETE REQUIREMENT', () async {
      var response = Future.value(
          ApiResponse(message: '', object: true, statusResponse: 200));

      when(_requerimentBlocMock.deleteRequirement(any))
          .thenAnswer((realInvocation) => response);

      expect(_requirementBloc.deleteRequirement(1), isA<Future<ApiResponse>>());
    });
  });
}
