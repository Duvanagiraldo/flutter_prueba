import 'dart:async';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:project_management_flutter/src/bloc/project_bloc.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/project_model.dart';
import 'project_bloc_test.mocks.dart';

// Command: flutter pub run build_runner build

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([ProjectBloc])
void main() {
  final _projectBlocMock = MockProjectBloc();
  final _projectBloc = ProjectBloc();

  var projectModel = Project(
      id: 1,
      name: 'App movil para el Software de Gestion Documental Docunet Web',
      client: 'Centro de datos Documental',
      startDate: '2020-09-05',
      finalDate: '2021-07-30',
      status: 'IN_PROCESS',
      observations:
          'Se tendra el apoyo del personal que conoce la version Web');

  group('PROJECT BLOC TEST', () {
    test('GET PROJECTS', () async {
      var response = Future.value(
          ApiResponse(message: '', object: [], statusResponse: 200));

      when(_projectBlocMock.getProjects())
          .thenAnswer((realInvocation) => response);

      expect(_projectBloc.getProjects(), isA<Future<ApiResponse>>());
    });

    test('GET PROJECT BY ID', () async {
      var response = Future.value(
          ApiResponse(message: '', object: projectModel, statusResponse: 200));

      when(_projectBlocMock.getProjectById(any))
          .thenAnswer((realInvocation) => response);

      expect(_projectBloc.getProjectById(1), isA<Future<ApiResponse>>());
    });

    test('STORE PROJECT', () async {
      var response = Future.value(
          ApiResponse(message: '', object: projectModel, statusResponse: 200));

      when(_projectBlocMock.storeProject(projectModel))
          .thenAnswer((realInvocation) => response);

      expect(
          _projectBloc.storeProject(projectModel), isA<Future<ApiResponse>>());
    });

    test('UPDATE PROJECT', () async {
      var response = Future.value(
          ApiResponse(message: '', object: projectModel, statusResponse: 200));

      when(_projectBlocMock.updateProject(projectModel))
          .thenAnswer((realInvocation) => response);

      expect(
          _projectBloc.updateProject(projectModel), isA<Future<ApiResponse>>());
    });

    test('DELETE PROJECT', () async {
      var response = Future.value(
          ApiResponse(message: '', object: true, statusResponse: 200));

      when(_projectBlocMock.deleteProject(any))
          .thenAnswer((realInvocation) => response);

      expect(_projectBloc.deleteProject(1), isA<Future<ApiResponse>>());
    });
  });
}
