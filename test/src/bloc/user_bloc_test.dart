import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:project_management_flutter/src/bloc/user_bloc.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/user_model.dart';

import 'user_bloc_test.mocks.dart';

// Command: flutter pub run build_runner build

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([UserBloc])
void main() {
  final _userBlocMock = MockUserBloc();
  final _userBloc = UserBloc();

  var userModel = User(
      id: 1,
      documentNumber: 123,
      firstName: 'Cristian Andrés',
      lastName: 'Loaiza',
      email: 'correo@correo.co',
      password: '***',
      role: 'ADMIN',
      status: 'ACTIVE');

  group('USER BLOC TEST', () {
    test('GET USERS', () async {
      var response = Future.value(
          ApiResponse(message: '', object: [], statusResponse: 200));

      when(_userBlocMock.getUsers()).thenAnswer((realInvocation) => response);

      expect(_userBloc.getUsers(), isA<Future<ApiResponse>>());
    });

    test('STORE USER', () async {
      var response = Future.value(
          ApiResponse(message: '', object: userModel, statusResponse: 200));

      when(_userBlocMock.storeUser(userModel))
          .thenAnswer((realInvocation) => response);

      expect(_userBloc.storeUser(userModel), isA<Future<ApiResponse>>());
    });

    test('GET USER BY ID', () async {
      var response = Future.value(
          ApiResponse(message: '', object: userModel, statusResponse: 200));

      when(_userBlocMock.getUserById(any))
          .thenAnswer((realInvocation) => response);

      expect(_userBloc.getUserById(1), isA<Future<ApiResponse>>());
    });

    test('UPDATE USER', () async {
      var response = Future.value(
          ApiResponse(message: '', object: userModel, statusResponse: 200));

      when(_userBlocMock.updateUser(userModel))
          .thenAnswer((realInvocation) => response);

      expect(_userBloc.updateUser(userModel), isA<Future<ApiResponse>>());
    });

    test('DELETE USER', () async {
      var response = Future.value(
          ApiResponse(message: '', object: true, statusResponse: 200));

      when(_userBlocMock.deleteUser(any))
          .thenAnswer((realInvocation) => response);

      expect(_userBloc.deleteUser(1), isA<Future<ApiResponse>>());
    });
  });
}
