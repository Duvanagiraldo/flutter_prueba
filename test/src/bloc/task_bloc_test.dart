import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:project_management_flutter/src/bloc/task_bloc.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/task_model.dart';

import 'task_bloc_test.mocks.dart';

@GenerateMocks([TaskBloc])
void main() {
  final _taskBlocMock = MockTaskBloc();
  final _taskBloc = TaskBloc();

  var taskModel = Task(
      id: 1,
      idRequirement: 1,
      name: 'Tarea de prueba',
      description: 'Tarea de prueba',
      finalDate: '2021-09-14',
      priority: 'MEDIUM',
      status: 'IN_PROCESS',
      observations: '',
      idResponsible: 1);

  group('TASK BLOC TEST', () {
    test('GET ALL TASK', () async {
      var response = Future.value(
          ApiResponse(message: '', object: [], statusResponse: 200));

      when(_taskBlocMock.getTask()).thenAnswer((_) => response);

      expect(_taskBloc.getTask(), isA<Future<ApiResponse>>());
    });

    test('STORE TASK', () async {
      var response = Future.value(
          ApiResponse(message: '', object: taskModel, statusResponse: 200));

      when(_taskBlocMock.storeTask(any))
          .thenAnswer((realInvocation) => response);

      expect(_taskBloc.storeTask(taskModel), isA<Future<ApiResponse>>());
    });

    test('GET TASK BY ID', () async {
      var response = Future.value(
          ApiResponse(message: '', object: taskModel, statusResponse: 200));

      when(_taskBlocMock.getTaskById(any))
          .thenAnswer((realInvocation) => response);

      expect(_taskBloc.getTaskById(1), isA<Future<ApiResponse>>());
    });

    test('UPDATE TASK', () async {
      var response = Future.value(
          ApiResponse(message: '', object: taskModel, statusResponse: 200));

      when(_taskBlocMock.updateTask(any))
          .thenAnswer((realInvocation) => response);

      expect(_taskBloc.updateTask(taskModel), isA<Future<ApiResponse>>());
    });

    test('DELETE TASK', () async {
      var response = Future.value(
          ApiResponse(message: '', object: true, statusResponse: 200));

      when(_taskBlocMock.deleteTask(any))
          .thenAnswer((realInvocation) => response);

      expect(_taskBloc.deleteTask(1), isA<Future<ApiResponse>>());
    });
  });
}
