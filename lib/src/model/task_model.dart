class Task {
  int? id;
  int? idRequirement;
  String? name;
  String? description;
  String? finalDate;
  String? priority;
  String? status;
  String? observations;
  int? idResponsible;

  Task(
      {this.id,
      this.idRequirement,
      this.name,
      this.description,
      this.finalDate,
      this.priority,
      this.status,
      this.observations,
      this.idResponsible});

  factory Task.fromJson(Map<String, dynamic> parsedJson) {
    return Task(
        id: parsedJson['id'],
        idRequirement: parsedJson['idRequirement'],
        name: parsedJson['name'],
        description: parsedJson['description'],
        finalDate: parsedJson['finalDate'],
        priority: parsedJson['priority'],
        status: parsedJson['status'],
        observations: parsedJson['observations'],
        idResponsible: parsedJson['idResponsible']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'idRequirement': idRequirement,
        'name': name,
        'descripcion': description,
        'finalDate': finalDate,
        'priority': priority,
        'status': status,
        'observations': observations,
        'idResponsible': idResponsible
      };
}
