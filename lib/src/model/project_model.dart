class Project {
  int? id;
  String? name;
  String? client;
  String? startDate;
  String? finalDate;
  String? status;
  String? observations;

  Project(
      {this.id,
      this.name,
      this.client,
      this.startDate,
      this.finalDate,
      this.status,
      this.observations});

  factory Project.fromJson(Map<String, dynamic> parsedJson) {
    return Project(
        id: parsedJson['id'],
        name: parsedJson['name'],
        client: parsedJson['client'],
        startDate: parsedJson['startDate'],
        finalDate: parsedJson['finalDate'],
        status: parsedJson['status'],
        observations: parsedJson['observations']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'client': client,
        'startDate': startDate,
        'finalDate': finalDate,
        'status': status,
        'observations': observations
      };

  Map<String, dynamic> toJsonProjectID() => {
        'id': id,
      };
}
