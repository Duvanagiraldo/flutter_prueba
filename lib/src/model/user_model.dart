class User {
  int? id;
  int? documentNumber;
  String? email;
  String? firstName;
  String? lastName;
  String? password;
  String? role;
  String? status;

  User(
      {this.id,
      this.documentNumber,
      this.email,
      this.firstName,
      this.lastName,
      this.password,
      this.role,
      this.status});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      id: parsedJson['id'],
      documentNumber: parsedJson['documentNumber'],
      email: parsedJson['email'],
      firstName: parsedJson['firstName'],
      lastName: parsedJson['lastName'],
      password: parsedJson['password'],
      role: parsedJson['role'],
      status: parsedJson['status'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'documentNumber': documentNumber,
        'email': email,
        'firstName': firstName,
        'lastName': lastName,
        'password': password,
        'role': role,
        'status': status,
      };

  Map<String, dynamic> toJsonUserId() => {
        'id': id,
      };
}
