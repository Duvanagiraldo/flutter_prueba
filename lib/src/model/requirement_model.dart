class Requirement {
  int? id;
  int? idProject;
  String? name;
  String? description;
  String? finalDate; // DATE
  String? priority;
  String? status;
  String? observations;

  Requirement(
      {this.id,
      this.idProject,
      this.name,
      this.description,
      this.finalDate,
      this.priority,
      this.status,
      this.observations});

  factory Requirement.fromJson(Map<String, dynamic> parsedJson) {
    return Requirement(
      id: parsedJson['id'],
      idProject: parsedJson['idProject'],
      name: parsedJson['name'],
      description: parsedJson['description'],
      finalDate: parsedJson['finalDate'],
      priority: parsedJson['priority'],
      status: parsedJson['status'],
      observations: parsedJson['observations'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'idProject': idProject,
        'name': name,
        'description': description,
        'finalDate': finalDate,
        'priority': priority,
        'status': status,
        'observations': observations,
      };

  Map<String, dynamic> toJsonRequirementId() => {
        'id': id,
      };
}
