import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_management_flutter/src/bloc/requirement_bloc.dart';
import 'package:project_management_flutter/src/model/requirement_model.dart';

class RequirementScreen extends StatefulWidget {
  const RequirementScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RequirementScreen();
}

class _RequirementScreen extends State<RequirementScreen> {
  List<Requirement> _requestList = [];

  @override
  void initState() {
    super.initState();

    var _requestBloc = RequirementBloc();

    _requestBloc.getRequirements().then((value) {
      setState(() {
        _requestList = value.object as List<Requirement>;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de requerimientos'),
      ),
      body: ListView.builder(
        itemCount: _requestList.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: const Icon(Icons.person),
            title: Text(_requestList[index].name! +
                ' ' +
                _requestList[index].description!),
            subtitle: Text(_requestList[index].finalDate!),
            trailing: Chip(
              label: Text(_requestList[index].priority!),
              backgroundColor: Colors.blue[800],
              labelStyle: const TextStyle(
                color: Colors.white,
              ),
            ),
          );
        },
      ),
    );
  }
}
