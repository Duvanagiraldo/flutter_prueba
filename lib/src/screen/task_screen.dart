import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_management_flutter/src/bloc/task_bloc.dart';
import 'package:project_management_flutter/src/model/task_model.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TaskScreen();
}

class _TaskScreen extends State<TaskScreen> {
  final TaskBloc _taskBloc = TaskBloc();

  @override
  void initState() {
    super.initState();
    _taskBloc.getTask();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de usuarios'),
      ),
      body: StreamBuilder(
          stream: _taskBloc.taskList,
          builder: (
            BuildContext context,
            AsyncSnapshot<List<Task>> snapshot,
          ) {
            final _taskList = snapshot.data ?? [];

            return ListView.builder(
                itemCount: _taskList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: const Icon(Icons.task),
                    title: Text(_taskList[index].name!),
                    subtitle: Text(_taskList[index].description!),
                    trailing: Chip(
                      label: Text(_taskList[index].status!),
                      backgroundColor: Colors.blue[800],
                      labelStyle: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
