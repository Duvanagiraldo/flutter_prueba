import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_management_flutter/src/screen/task_screen.dart';
import 'package:project_management_flutter/src/screen/user_screen.dart';
import 'package:project_management_flutter/src/screen/project_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  void _pushScreen(BuildContext context, Widget screen) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => screen),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Sistema de Gestión de Proyectos'),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: const Icon(Icons.work),
              title: const Text('Gestión de Proyectos'),
              onTap: () => _pushScreen(context, const ProjectScreen()),
            ),
            ListTile(
              leading: const Icon(Icons.people),
              title: const Text('Gestión de Usuarios'),
              onTap: () => _pushScreen(context, const UserScreen()),
            ),
            ListTile(
              leading: const Icon(Icons.task),
              title: const Text('Gestión de tareas'),
              onTap: () => _pushScreen(context, const TaskScreen()),
            ),
          ],
        ));
  }
}
