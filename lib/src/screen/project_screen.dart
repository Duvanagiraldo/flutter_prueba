import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_management_flutter/src/bloc/project_bloc.dart';
import 'package:project_management_flutter/src/model/project_model.dart';

class ProjectScreen extends StatefulWidget {
  const ProjectScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProjectScreen();
}

class _ProjectScreen extends State<ProjectScreen> {
  final ProjectBloc _projectBloc = ProjectBloc();

  @override
  void initState() {
    super.initState();

    _projectBloc.getProjects();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de Projectos'),
      ),
      body: StreamBuilder(
          stream: _projectBloc.projectList,
          builder: (
            BuildContext context,
            AsyncSnapshot<List<Project>> snapshot,
          ) {
            final _projectList = snapshot.data ?? [];

            return ListView.builder(
                itemCount: _projectList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: const Icon(Icons.work),
                    title: Text(_projectList[index].name!),
                    subtitle: Text(_projectList[index].client!),
                    trailing: Chip(
                      label: Text(_projectList[index].status!),
                      backgroundColor: Colors.blue[800],
                      labelStyle: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
