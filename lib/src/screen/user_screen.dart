import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_management_flutter/src/bloc/user_bloc.dart';
import 'package:project_management_flutter/src/model/user_model.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UserScreen();
}

class _UserScreen extends State<UserScreen> {
  final UserBloc _userBloc = UserBloc();

  @override
  void initState() {
    super.initState();

    _userBloc.getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de usuarios'),
      ),
      body: StreamBuilder(
          stream: _userBloc.userList,
          builder: (
            BuildContext context,
            AsyncSnapshot<List<User>> snapshot,
          ) {
            final _userList = snapshot.data ?? [];

            return ListView.builder(
                itemCount: _userList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: const Icon(Icons.person),
                    title: Text(_userList[index].firstName! +
                        ' ' +
                        _userList[index].lastName!),
                    subtitle: Text(_userList[index].email!),
                    trailing: Chip(
                      label: Text(_userList[index].role!),
                      backgroundColor: Colors.blue[800],
                      labelStyle: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
