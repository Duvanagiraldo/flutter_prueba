import 'dart:convert';
import 'dart:io';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/error_api_response_model.dart';
import 'package:project_management_flutter/src/model/project_model.dart';
import 'package:project_management_flutter/src/utils/constants.dart';
import 'package:http/http.dart' as http;

class ProjectApiService {
  Uri _uri(String param) =>
      Uri.parse(Constants.urlBackend + Constants.projectUrl + param);

  late Project? _project;
  ErrorApiResponse? _error;

  ProjectApiService();

  Future<ApiResponse> getAllProjects(String accessToken) async {
    var listProjects = [];
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(''), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((project) {
        listProjects.add(Project.fromJson(project));

        return project;
      });
      apiResponse.object = listProjects;
    } else {
      resBody['message'] = 'Error al consultar el listado de Proyectos';

      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> getProjectById(int projectId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(projectId.toString()), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      var project = Project.fromJson(resBody);
      apiResponse.object = project;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeProject(Project project, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(project.toJson());

    var res = await http.post(
      _uri(''),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _project = Project.fromJson(resBody);
      apiResponse.object = _project;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateProject(Project project, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(project.toJson());

    var res = await http.put(
      _uri(project.id.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _project = Project.fromJson(resBody);
      apiResponse.object = _project;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteProject(int? projectId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.delete(
      _uri(projectId.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Project.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
