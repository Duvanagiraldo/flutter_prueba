import 'dart:convert';
import 'dart:io';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/error_api_response_model.dart';
import 'package:project_management_flutter/src/model/user_model.dart';
import 'package:project_management_flutter/src/utils/constants.dart';
import 'package:http/http.dart' as http;

class UserApiService {
  Uri _uri(String param) =>
      Uri.parse(Constants.urlBackend + Constants.userUrl + param);

  late User? _user;

  UserApiService();

  Future<ApiResponse> getAllUsers(String accessToken) async {
    var listUsers = [];
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(''), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((user) {
        listUsers.add(User.fromJson(user));

        return user;
      });
      apiResponse.object = listUsers;
    } else {
      apiResponse.message = 'Error al consultar el listado de usuarios';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> getUserById(int userId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(userId.toString()), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = User.fromJson(resBody);
    } else {
      apiResponse.message = 'Error al consultar el usuario';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeUser(User user, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(user.toJson());

    var res = await http.post(
      _uri(''),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      apiResponse.message = 'Error al crear el usuario';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateUser(User user, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(user.toJson());

    var res = await http.put(
      _uri(user.id.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _user = User.fromJson(resBody);
      apiResponse.object = _user;
    } else {
      apiResponse.message = 'Error al actualizar el usuario';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteUser(int userId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.delete(
      _uri(userId.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = resBody;
    } else {
      apiResponse.message = 'Error al eliminar el usuario';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }
}
