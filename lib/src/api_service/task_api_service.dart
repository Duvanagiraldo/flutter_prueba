import 'dart:convert';
import 'dart:io';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/error_api_response_model.dart';
import 'package:project_management_flutter/src/model/task_model.dart';
import 'package:http/http.dart' as http;
import 'package:project_management_flutter/src/utils/constants.dart';

class TaskApiService {
  ErrorApiResponse? _error;

  TaskApiService();
  late Task? _task;

  Uri _uri(String param) =>
      Uri.parse(Constants.urlBackend + Constants.taskUrl + param);

  Future<ApiResponse> getAllTask(String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(''), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      var taskList = [];

      resBody.forEach((task) {
        taskList.add(Task.fromJson(task));
        return task;
      });

      apiResponse.object = taskList;
    } else {
      resBody['message'] = 'prueba';
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getTaskById(int taskId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(taskId.toString()), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Task.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> storeTask(Task task, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(task.toJson());

    var res = await http.post(
      _uri(''),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _task = Task.fromJson(resBody);
      apiResponse.object = _task;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateTask(Task task, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(task.toJson());

    var res = await http.put(
      _uri(task.id.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Task.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteUser(int taskId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.delete(
      _uri(taskId.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Task.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }

    return apiResponse;
  }
}
