import 'dart:convert';
import 'dart:io';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/error_api_response_model.dart';
import 'package:project_management_flutter/src/model/requirement_model.dart';
import 'package:project_management_flutter/src/utils/constants.dart';
import 'package:http/http.dart' as http;

class RequirementApiService {
  Uri _uri(String param) =>
      Uri.parse(Constants.urlBackend + Constants.requirementUrl + param);

  late Requirement? _request;

  RequirementApiService();

  Future<ApiResponse> getAllRequirements(String accessToken) async {
    var listRequirements = [];
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(''), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((request) {
        listRequirements.add(Requirement.fromJson(request));

        return request;
      });
      apiResponse.object = listRequirements;
    } else {
      apiResponse.message = 'Error al consultar el listado de requerimientos';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> getRequirementById(
      int requestId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.get(_uri(requestId.toString()), headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = jsonDecode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      var requirement = Requirement.fromJson(resBody);
      apiResponse.object = requirement;
    } else {
      apiResponse.message = 'Error al consultar el   requerimiento';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeRequirement(
      Requirement request, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(request.toJson());

    var res = await http.post(
      _uri(''),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Requirement.fromJson(resBody);
    } else {
      apiResponse.message = 'Error al crear  el requerimiento';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateRequirement(
      Requirement request, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var body = jsonEncode(request.toJson());

    var res = await http.put(
      _uri(request.id.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _request = Requirement.fromJson(resBody);
      apiResponse.object = _request;
    } else {
      apiResponse.message = 'Error al actualizar el  requerimiento';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteRequirement(
      int requestId, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);

    var res = await http.delete(
      _uri(requestId.toString()),
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
    );

    var resBody = jsonDecode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      apiResponse.object = Requirement.fromJson(resBody);
    } else {
      apiResponse.message = 'Error al eliminar el  requerimiento';
      apiResponse.object = ErrorApiResponse.fromJson(resBody);
    }

    return apiResponse;
  }
}
