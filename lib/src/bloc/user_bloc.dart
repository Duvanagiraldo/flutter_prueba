import 'dart:async';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/user_model.dart';
import 'package:project_management_flutter/src/repository/user_repository.dart';

class UserBloc {
  // BuildContext? _context;

  final _repository = UserRepository();
  final _userListController = StreamController<List<User>>();
  final _userController = StreamController<User>();

  Stream<List<User>> get userList => _userListController.stream;

  Stream<User> get user => _userController.stream;

  UserBloc();

  // UserBloc(BuildContext context) {
  //   _context = context;
  // }

  Future<ApiResponse> getUsers() async {
    var apiResponse = await _repository.getAllUsers();

    if (apiResponse.statusResponse == 200) {
      _userListController.add(apiResponse.object as List<User>);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> getUserById(int userId) async {
    var apiResponse = await _repository.getUserById(userId);

    if (apiResponse.statusResponse == 200) {
      _userController.add(apiResponse.object as User);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeUser(User user) async {
    var apiResponse = await _repository.storeUser(user);

    if (apiResponse.statusResponse == 200) {
      var _newUser = apiResponse.object as User;
      _userController.add(_newUser);

      userList.listen((_userList) {
        _userList.add(_newUser);
        _userListController.add(_userList);
      });
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateUser(User user) async {
    var apiResponse = await _repository.updateUser(user);

    if (apiResponse.statusResponse == 200) {
      var _updatedUser = apiResponse.object as User;
      _userController.add(_updatedUser);

      userList.listen((_userList) {
        for (var _user in _userList) {
          if (_user.id == _updatedUser.id) {
            _user = _updatedUser;
          }
        }
        _userListController.add(_userList);
      });
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteUser(int userId) async {
    var apiResponse = await _repository.deleteUser(userId);

    if (apiResponse.statusResponse == 200) {
      userList.listen((_userList) {
        var _newUserList = <User>[];

        for (var _user in _userList) {
          if (_user.id != userId) {
            _newUserList.add(_user);
          }
        }

        _userListController.add(_newUserList);
      });
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  void dispose() {
    _userListController.close();
    _userController.close();
  }
}
