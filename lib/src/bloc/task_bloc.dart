import 'dart:async';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/task_model.dart';
import 'package:project_management_flutter/src/repository/task_repository.dart';

class TaskBloc {
  // BuildContext? _context;
  final _taskRepository = TaskRepository();
  final _taskListController = StreamController<List<Task>>();
  final _taskController = StreamController<Task>();

  Stream<List<Task>> get taskList => _taskListController.stream;
  Stream<Task> get user => _taskController.stream;

  TaskBloc();

  Future<ApiResponse> getTask() async {
    var apiResponse = await _taskRepository.getAllTask();
    if (apiResponse.statusResponse == 200) {
      _taskListController.add(apiResponse.object as List<Task>);
    } else {
      throw Exception(apiResponse.message);
    }
    return apiResponse;
  }

  Future<ApiResponse> getTaskById(int taskId) async {
    var apiResponse = await _taskRepository.getTaskById(taskId);
    if (apiResponse.statusResponse == 200) {
      _taskController.add(apiResponse.object as Task);
    } else {
      throw Exception(apiResponse.message);
    }
    return apiResponse;
  }

  Future<ApiResponse> storeTask(Task task) async {
    var apiResponse = await _taskRepository.storeTask(task);

    if (apiResponse.statusResponse == 200) {
      var _newTask = apiResponse.object as Task;
      _taskController.add(_newTask);

      taskList.listen((_taskList) {
        _taskList.add(_newTask);
        _taskListController.add(_taskList);
      });
    } else {
      throw Exception(apiResponse.message);
    }
    return apiResponse;
  }

  Future<ApiResponse> updateTask(Task task) async {
    var apiResponse = await _taskRepository.updateTask(task);
    if (apiResponse.statusResponse == 200) {
      _taskController.add(apiResponse.object as Task);
    } else {
      throw Exception(apiResponse.message);
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteTask(int idTask) async {
    var apiResponse = await _taskRepository.deleteTask(idTask);
    if (apiResponse.statusResponse == 200) {
      _taskController.add(apiResponse.object as Task);
    } else {
      throw Exception(apiResponse.message);
    }
    return apiResponse;
  }
}
