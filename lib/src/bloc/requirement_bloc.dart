import 'dart:async';

import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/requirement_model.dart';
import 'package:project_management_flutter/src/repository/requirement_repository.dart';

class RequirementBloc {
  // BuildContext? _context;

  final _repository = RequirementRepository();
  final _requirementListController = StreamController<List<Requirement>>();
  final _requirementController = StreamController<Requirement>();

  Stream<List<Requirement>> get requerimentList =>
      _requirementListController.stream;

  Stream<Requirement> get request => _requirementController.stream;

  RequirementBloc();

  Future<ApiResponse> getRequirements() async {
    var apiResponse = await _repository.getAllRequirements();

    if (apiResponse.statusResponse == 200) {
      _requirementListController.add(apiResponse.object as List<Requirement>);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> getRequirementById(int requestId) async {
    var apiResponse = await _repository.getRequirementById(requestId);

    if (apiResponse.statusResponse == 200) {
      _requirementController.add(apiResponse.object as Requirement);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeRequirement(Requirement request) async {
    var apiResponse = await _repository.storeRequirement(request);

    if (apiResponse.statusResponse == 200) {
      var _newRequirement = apiResponse.object as Requirement;
      _requirementController.add(_newRequirement);

      requerimentList.listen((_requestList) {
        _requestList.add(_newRequirement);
        _requirementListController.add(_requestList);
      });
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateRequirement(Requirement request) async {
    var apiResponse = await _repository.updateRequirement(request);

    if (apiResponse.statusResponse == 200) {
      var _updatedRequirement = apiResponse.object as Requirement;
      _requirementController.add(_updatedRequirement);

      var _requestList = requerimentList as List<Requirement>;

      for (var _request in _requestList) {
        if (_request.id == _updatedRequirement.id) {
          _request = _updatedRequirement;
        }
      }
      _requirementListController.add(_requestList);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteRequirement(int requestId) async {
    var apiResponse = await _repository.deleteRequirement(requestId);

    if (apiResponse.statusResponse == 200) {
      var _requestList = requerimentList as List<Requirement>;
      var _newRequirementList = [];

      for (var _request in _requestList) {
        if (_request.id != requestId) {
          _newRequirementList.add(_request);
        }
      }
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  void dispose() {
    _requirementListController.close();
    _requirementController.close();
  }
}
