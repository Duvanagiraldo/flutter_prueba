import 'dart:async';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/project_model.dart';
import 'package:project_management_flutter/src/repository/project_repository.dart';

class ProjectBloc {
  // BuildContext? _context;

  final _repository = ProjectRepository();
  final _projectListController = StreamController<List<Project>>();
  final _projectController = StreamController<Project>();

  Stream<List<Project>> get projectList => _projectListController.stream;

  Stream<Project> get project => _projectController.stream;

  ProjectBloc();

  Future<ApiResponse> getProjects() async {
    var apiResponse = await _repository.getAllProjects();

    if (apiResponse.statusResponse == 200) {
      _projectListController.add(apiResponse.object as List<Project>);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> getProjectById(int projectId) async {
    var apiResponse = await _repository.getProjectById(projectId);

    if (apiResponse.statusResponse == 200) {
      _projectController.add(apiResponse.object as Project);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeProject(Project project) async {
    var apiResponse = await _repository.storeProject(project);

    if (apiResponse.statusResponse == 200) {
      var _newProject = apiResponse.object as Project;
      _projectController.add(_newProject);

      projectList.listen((_projectList) {
        _projectList.add(_newProject);
        _projectListController.add(_projectList);
      });
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateProject(Project project) async {
    var apiResponse = await _repository.updateProject(project);

    if (apiResponse.statusResponse == 200) {
      var _updatedProject = apiResponse.object as Project;
      _projectController.add(_updatedProject);

      var _projectList = projectList as List<Project>;

      for (var _project in _projectList) {
        if (_project.id == _updatedProject.id) {
          _project = _updatedProject;
        }
      }
      _projectListController.add(_projectList);
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteProject(int? projectId) async {
    var apiResponse = await _repository.deleteProject(projectId);

    if (apiResponse.statusResponse == 200) {
      var _projectList = projectList as List<Project>;
      var _newProjectList = [];

      for (var _project in _projectList) {
        if (_project.id != projectId) {
          _newProjectList.add(_project);
        }
      }
    } else {
      throw Exception(apiResponse.message);
    }

    return apiResponse;
  }

  void dispose() {
    _projectListController.close();
    _projectController.close();
  }
}
