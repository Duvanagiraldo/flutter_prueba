import 'package:project_management_flutter/src/api_service/project_api_service.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/project_model.dart';
import 'package:project_management_flutter/src/utils/manage_access_token.dart';

class ProjectRepository extends ManageAccessToken {
  final projectApiService = ProjectApiService();

  Future<ApiResponse> getAllProjects() =>
      projectApiService.getAllProjects(accessToken);

  Future<ApiResponse> getProjectById(int projectId) =>
      projectApiService.getProjectById(projectId, accessToken);

  Future<ApiResponse> storeProject(Project project) =>
      projectApiService.storeProject(project, accessToken);

  Future<ApiResponse> updateProject(Project project) =>
      projectApiService.updateProject(project, accessToken);

  Future<ApiResponse> deleteProject(int? projectId) =>
      projectApiService.deleteProject(projectId, accessToken);
}
