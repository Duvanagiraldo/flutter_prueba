import 'package:project_management_flutter/src/api_service/requirement_api_service.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/requirement_model.dart';
import 'package:project_management_flutter/src/utils/manage_access_token.dart';

class RequirementRepository extends ManageAccessToken {
  final requestApiService = RequirementApiService();

  Future<ApiResponse> getAllRequirements() =>
      requestApiService.getAllRequirements(accessToken);

  Future<ApiResponse> getRequirementById(int requestId) =>
      requestApiService.getRequirementById(requestId, accessToken);

  Future<ApiResponse> storeRequirement(Requirement request) =>
      requestApiService.storeRequirement(request, accessToken);

  Future<ApiResponse> updateRequirement(Requirement request) =>
      requestApiService.updateRequirement(request, accessToken);

  Future<ApiResponse> deleteRequirement(int requestId) =>
      requestApiService.deleteRequirement(requestId, accessToken);
}
