import 'package:project_management_flutter/src/api_service/task_api_service.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/task_model.dart';
import 'package:project_management_flutter/src/utils/manage_access_token.dart';

class TaskRepository extends ManageAccessToken {
  final taskApiService = TaskApiService();

  Future<ApiResponse> getAllTask() => taskApiService.getAllTask(accessToken);

  Future<ApiResponse> getTaskById(int taskId) =>
      taskApiService.getTaskById(taskId, accessToken);
  Future<ApiResponse> storeTask(Task task) =>
      taskApiService.storeTask(task, accessToken);
  Future<ApiResponse> updateTask(Task task) =>
      taskApiService.updateTask(task, accessToken);

  Future<ApiResponse> deleteTask(int taskId) =>
      taskApiService.deleteUser(taskId, accessToken);
}
