import 'package:project_management_flutter/src/api_service/user_api_service.dart';
import 'package:project_management_flutter/src/model/api_response_model.dart';
import 'package:project_management_flutter/src/model/user_model.dart';
import 'package:project_management_flutter/src/utils/manage_access_token.dart';

class UserRepository extends ManageAccessToken {
  final userApiService = UserApiService();

  Future<ApiResponse> getAllUsers() => userApiService.getAllUsers(accessToken);

  Future<ApiResponse> getUserById(int userId) =>
      userApiService.getUserById(userId, accessToken);

  Future<ApiResponse> storeUser(User user) =>
      userApiService.storeUser(user, accessToken);

  Future<ApiResponse> updateUser(User user) =>
      userApiService.updateUser(user, accessToken);

  Future<ApiResponse> deleteUser(int userId) =>
      userApiService.deleteUser(userId, accessToken);
}
