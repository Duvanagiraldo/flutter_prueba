class Constants {
  static const String urlBackend =
      'http://192.168.0.44:8080/api/project_management';
  static const String contentTypeHeader = 'application/json';
  static const String authorizationHeader = 'Bearer';

  // User
  static const String userUrl = '/user/';
  // Project
  static const String projectUrl = '/project/';
  // Requirement
  static const String requirementUrl = '/requirement/';
  // Task
  static const String taskUrl = '/task/';
}
